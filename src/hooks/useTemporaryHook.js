import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { get_data } from "../redux/actions/actions";

const useTemporaryHook = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(get_data());
  }, []);
};
export default useTemporaryHook;
