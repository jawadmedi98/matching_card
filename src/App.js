import React, { useEffect, useState } from "react";
import Header from "./components/Header";
import GlobalStyle, { Container, Field } from "./style/GlobalStyle";
import { theme } from "./style/theme";
import { ThemeProvider } from "styled-components";
import Board from "./components/Board";
import TableInfo from "./components/TableInfo";
import useTemporaryHook from "./hooks/useTemporaryHook";
import { useDispatch } from "react-redux";
import { numberCardes } from "./redux/actions/actions";

function App() {
  const [animation, setAnimation] = useState(true);
  const dispatch = useDispatch();
  useTemporaryHook();

  useEffect(() => {
    let countDown = setTimeout(() => {
      setAnimation(false);
      clearTimeout(countDown);
      dispatch(numberCardes("8"));
    }, 3700);
  }, []);
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <Container>
        <Header />
        <Field>
          <Board animation={animation} />
          <TableInfo />
        </Field>
      </Container>
    </ThemeProvider>
  );
}

export default App;
