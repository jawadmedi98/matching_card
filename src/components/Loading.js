import React from "react";
import { Main, Scope } from "../style/LoadingStyle";

function Loading() {
  return (
    <Main>
      <Scope>
        <div className="floor"></div>
        <div className="box">
          <p className="front">?</p>
          <p className="left">?</p>
          <p className="right">?</p>
          <p className="back">?</p>

          <p className="down">?</p>
        </div>
        <div className="ball"></div>
      </Scope>
    </Main>
  );
}

export default Loading;
