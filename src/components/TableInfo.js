import React, { useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  TabInfo,
  Score,
  Options,
  Hr,
  H3,
  Select,
  Icon,
  DropMenu,
  Btn,
  Done,
} from "../style/TableInfoSty";
import { numberCardes } from "../redux/actions/actions";
import gameSelector from "../redux/selectors/gameSelector";

function TableInfo() {
  const [show, setShow] = useState(false);
  const [gms] = useState([6, 8, 10, 12, 14]);

  const { nCards, tries, score, done } = useSelector(gameSelector);
  const pairsRef = useRef("");
  const dispatch = useDispatch();

  const numSquares = (e) => {
    dispatch(numberCardes(e.target.value));
  };
  const restartGame = () => {
    const value = pairsRef.current.value;
    dispatch(numberCardes(value));
  };

  return (
    <TabInfo>
      <Score>
        <H3>Score</H3>
        <h1>
          {" "}
          <span className="number">{score}</span>
          <span className="slash">/</span>
          {pairsRef.current.value}
        </h1>
        <p>Tries: {tries}</p>
      </Score>
      <Hr></Hr>
      <Options>
        <H3>Options</H3>
        <Select>
          Size
          <div className="arrow">
            <select onChange={numSquares} value={nCards || "8"} ref={pairsRef}>
              {gms.map((ite, i) => (
                <option value={ite} key={i}>
                  {ite} pairs
                </option>
              ))}
            </select>
          </div>
        </Select>
        <Btn onClick={restartGame}>Restart</Btn>
      </Options>
      <Icon onClick={() => setShow(!show)}>
        <i className="fas fa-cog"></i>
        <DropMenu className="DropMenu" show={show}>
          <div className="games">
            {gms.map((it, i) => (
              <div
                className="game"
                onClick={() => dispatch(numberCardes(it))}
                key={i}
              >
                {it}
              </div>
            ))}
          </div>
          <Btn onClick={restartGame}>Restart</Btn>
        </DropMenu>
      </Icon>
      <Done className="done" done={done}>
        <h2>Congrat!</h2>
        You solved the game in {tries} attempts
      </Done>
    </TabInfo>
  );
}

export default TableInfo;
