import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { rotate_Item } from "../redux/actions/actions";
import gameSelector from "../redux/selectors/gameSelector";
import { Card, Face, Back, Table } from "../style/BoardStyle";
import Loading from "./Loading";

function Board({ animation }) {
  const dispatch = useDispatch();
  const { data, loading, ids } = useSelector(gameSelector);

  const handleRotate = (ind) => {
    if (ids.length < 2) {
      dispatch(rotate_Item(ind));
    }
  };

  return loading || animation ? (
    <Loading />
  ) : (
    <Table leng={data.length} events={ids.length == 2}>
      {data.map((item, i) => {
        return (
          <Card
            key={i}
            rotate1={item.rotate}
            onClick={() => handleRotate(i)}
            visible={item.visible}
          >
            <Face>
              <img src={item.images} />
            </Face>
            <Back visible={item.visible}>?</Back>
          </Card>
        );
      })}
    </Table>
  );
}

export default Board;
