import React from "react";
import { H2 } from "../style/GlobalStyle";

function Header() {
  return (
    <div>
      <H2>Find the pairs</H2>
    </div>
  );
}

export default Header;
