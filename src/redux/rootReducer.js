import { combineReducers } from "redux";
import gameReducer from "./reducers/gameReducer";

const rootReducers = combineReducers({
  game: gameReducer,
});
export default rootReducers;
