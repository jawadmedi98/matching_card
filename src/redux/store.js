import { createStore, applyMiddleware } from "redux";
import rootReducers from "./rootReducer";
import thunk from "redux-thunk";
// import logger from 'redux-logger'

const store = createStore(rootReducers, applyMiddleware(thunk));

export default store;
