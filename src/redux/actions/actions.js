import types from "./types";
import axios from "axios";

/**
 * @function get_Data
 */
export const get_data = () => (dispatch, getState) => {
  dispatch({
    type: types.DATA_REQUEST,
  });
  axios
    .get("data.json")
    .then((res) => {
      let testing = res.data.map((item) => {
        return { ...item, rotate: true };
      });
      dispatch({
        type: types.GET_DATA_SUCCESS,
        payload: testing,
      });
    })
    .catch((err) => {
      dispatch({
        type: types.GET_DATA_FAILURE,
        payload: err,
      });
    });
};

/**
 * @function rotate_Item
 *
 */

export const rotate_Item = (ind) => (dispatch, getState) => {
  const list = getState().game.list;
  let score = getState().game.score;

  const newList = list.map((item, i) =>
    ind === i ? { ...item, rotate: true } : item
  );

  let idss = [];
  idss = [...idss, ind];

  dispatch({
    type: types.ROTATE_ITEM,
    payload: newList,
  });
  dispatch({
    type: types.FILL_IDS,
    payload: idss,
  });
  let theIds = getState().game.ids;
  let newll = list.map((ite) =>
    theIds.includes(list.indexOf(ite)) ? { ...ite, rotate: false } : ite
  );

  if (theIds.length === 2) {
    if (score * 2 + 2 == list.length) {
      dispatch({
        type: types.THE_GAME_IS_DONE,
      });
    }
    let selectedItems = list.filter((item) =>
      theIds.includes(list.indexOf(item)) ? item : null
    );
    let nTries = getState().game.tries;

    dispatch({
      type: types.COUNT_TRIES,
      payload: nTries + 1,
    });
    if (selectedItems[0].name === selectedItems[1].name) {
      dispatch({
        type: types.FOUND_PAIRS,
        payload: score + 1,
      });
      setTimeout(() => {
        dispatch({
          type: types.CLEAR_IDS,
        });
        let newA = list.map((ite) =>
          theIds.includes(list.indexOf(ite)) ? { visible: false } : ite
        );
        dispatch({
          type: types.REMOVE_PAIRS,
          payload: newA,
        });
      }, 1000);
    } else {
      setTimeout(() => {
        dispatch({
          type: types.CLEAR_IDS,
        });
        dispatch({
          type: types.BACK_ITEM,
          payload: newll,
        });
      }, 1000);
    }
  }
};

export const numberCardes = (val) => (dispatch, getState) => {
  const allData = getState().game.allData;
  let value = parseInt(val);
  let items = allData.slice(0, value);
  let newList = [...items, ...items];
  for (let i = 0; i < newList.length; i++) {
    const rand = Math.floor(Math.random() * newList.length);
    [newList[i], newList[rand]] = [newList[rand], newList[i]];
  }
  dispatch({
    type: types.RESET_TRIES,
  });
  dispatch({
    type: types.RESET_SCORE,
  });
  dispatch({
    type: types.NUMBER_OF_CARDS,
    payload: newList,
  });
  dispatch({
    type: types.SET_VAL_CARDS,
    payload: val,
  });
  const list = getState().game.list;
  let newLis = list.map((item) => {
    return { ...item, rotate: false };
  });
  setTimeout(() => {
    dispatch({
      type: types.TAKE_LOOK,
      payload: newLis,
    });
  }, 3000);
};
