import { createSelector } from "reselect";

const gameSelector = createSelector(
  (state) => state.game,
  (gameState) => {
    return {
      loading: gameState.loading,
      data: gameState.list,
      error: gameState.error,
      ids: gameState.ids,
      nCards: gameState.nCards,
      tries: gameState.tries,
      score: gameState.score,
      done: gameState.done,
    };
  }
);
export default gameSelector;
