import types from "../actions/types";

const initialState = {
  loading: false,
  list: [],
  error: "",
  ids: [],
  allData: [],
  nCards: null,
  tries: 0,
  score: 0,
  done: false,
};

export default function gameReducer(state = initialState, action) {
  switch (action.type) {
    case types.DATA_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case types.GET_DATA_SUCCESS:
      return {
        ...state,
        allData: action.payload,
        loading: false,
      };
    case types.GET_DATA_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case types.SHUFFLE_ITEMS:
      return {
        ...state,
        list: action.payload,
      };
    case types.TAKE_LOOK:
      return {
        ...state,
        list: action.payload,
      };
    case types.ROTATE_ITEM:
      return {
        ...state,
        list: action.payload,
      };
    case types.FILL_IDS:
      return {
        ...state,
        ids: [...state.ids, ...action.payload],
      };
    case types.BACK_ITEM:
      return {
        ...state,
        list: action.payload,
      };
    case types.CLEAR_IDS:
      return {
        ...state,
        ids: [],
      };
    case types.REMOVE_PAIRS:
      return {
        ...state,
        list: action.payload,
      };
    case types.NUMBER_OF_CARDS:
      return {
        ...state,
        list: action.payload,
      };
    case types.SET_VAL_CARDS:
      return {
        ...state,
        nCards: action.payload,
      };

    case types.COUNT_TRIES:
      return {
        ...state,
        tries: action.payload,
      };
    case types.RESET_TRIES:
      return {
        ...state,
        tries: 0,
      };
    case types.FOUND_PAIRS:
      return {
        ...state,
        score: action.payload,
      };
    case types.RESET_SCORE:
      return {
        ...state,
        score: 0,
        done: false,
      };
    case types.THE_GAME_IS_DONE:
      return {
        ...state,
        done: true,
      };
    default:
      return state;
  }
}
