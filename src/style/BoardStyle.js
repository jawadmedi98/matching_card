import styled, { css } from "styled-components";

export const Table = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: flex-start;
  justify-content: center;
  align-content: center;
  min-height: 450px;
  max-height: 100%;
  border-radius: 4px;
  pointer-events: ${(props) => (props.events ? "none" : "auto")};
  div,
  img {
    border-radius: 5px;
  }
  width: ${(props) =>
    props.leng === 16 || props.leng === 12
      ? "460px"
      : props.leng === 20
      ? "570px"
      : props.leng === 24
      ? "740px"
      : "800px"};
  @media (max-width: 800px) {
    ${(props) =>
      props.leng === 28 &&
      css`
        & > div {
          width: 89px;
          height: 89px;
          margin: 5px;
        }
      `}
  }
  @media (max-width: 700px) {
    ${(props) =>
      props.leng <= 24 &&
      css`
        & > div {
          width: 87px;
          height: 87px;
        }
      `}
  }
  @media (max-width: 420px) {
    ${(props) =>
      props.leng <= 28 &&
      css`
        & > div {
          width: 67px;
          height: 67px;
          margin: 3px;
          box-shadow: none;
        }
      `}
  }
`;
export const Card = styled.div`
  position: relative;
  width: 100px;
  height: 100px;
  transform-style: preserve-3d;
  box-shadow: ${(props) =>
    props.visible
      ? "5px 5px 8px 4px rgb(223,228,230),-5px -5px 8px 4px rgb(223,228,230)"
      : ""};
  transition: all 0.4s ease-in-out;
  transform: rotateY(${(props) => (props.rotate1 ? "180deg" : "0")});
  visibility: ${(props) => (props.visible ? "visible" : "hidden")};
  cursor: pointer;
  user-select: none;
  pointer-events: ${(props) => (props.rotate1 ? "none" : "auto")};
  border-radius: 4px;
  margin: 7px;
`;
export const Face = styled.div`
  transform: rotateY(180deg);
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  border-radius: 4px;
  height: 100%;
  img {
    display: block;
    width: 100%;
  }
`;
export const Back = styled.div`
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  color: #fff;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  border-radius: 4px;
  visibility: ${(props) => (props.visible ? "visible" : "hidden")};
  backface-visibility: hidden;
  background-color: rgb(24, 144, 255);
  font-size: 30px;
  transform: rotateX(0deg);
`;
