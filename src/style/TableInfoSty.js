import styled from "styled-components";

export const TabInfo = styled.div`
  background: #fff;
  width: 200px;
  height: 265px;
  border: 1px solid #ddd;
  margin-left: 50px;
  border-radius: 3px;
  padding: 10px;
  text-align: left;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  color: #111;
  font-size: 16px;
  @media (max-width: 1060px) {
    /* justify-content: center; */
    width: 90%;
    height: 50px;
    flex-direction: row;
    flex-wrap: wrap;
    margin: 0 auto 20px;
    hr {
      display: none;
    }
  }
  @media (max-width: 698px) {
    width: 98%;
  }
`;
export const Score = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  max-width: 50%;
  min-width: 180px;

  height: 35%;
  @media (max-width: 1060px) {
    width: calc(100% / 3);
    height: 100%;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    flex-wrap: wrap;
    h1 {
      margin-left: -20px;
    }
  }
  h1 {
    font-size: 20px;
  }
  .number {
    color: #1890ff;
    margin-right: 4px;
  }
  .slash {
    margin-right: 4px;
  }
  @media (max-width: 698px) {
  }
`;
export const H3 = styled.p`
  font-weight: bolder;
  padding-right: 10px;
`;
export const Hr = styled.hr`
  width: 50%;
  border: 1px solid #ddd;
  outline: none;
`;
export const Options = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: flex-start;
  height: 45%;
  width: 100%;
  @media (max-width: 1060px) {
    width: calc(100% - 100% / 3);
    height: 100%;
    flex-direction: row;
    align-items: center;
    justify-content: flex-start;
    padding: 0 0 0 0px;
    width: 40%;
    p {
      display: none;
    }
  }
  @media (max-width: 768px) {
    display: none;
  }
`;
export const Btn = styled.button`
  background-color: #1890ff;
  border: none;
  padding: 6px 12px;
  color: #fff;
  border-radius: 2px;
  cursor: pointer;
`;
export const Select = styled.div`
  width: 100%;
  .arrow {
    width: 100px;
    height: 25px;
    margin-right: 10px;
    position: relative;
    display: inline-block;
    margin-left: 30px;
  }
  .arrow::after {
    content: "";
    z-index: 1;
    width: 5px;
    height: 5px;
    border-bottom: 1px solid #777;
    border-left: 1px solid #777;
    display: block;
    position: absolute;
    transform: rotate(-45deg);
    top: 8px;
    right: 10px;
  }
  select {
    display: block;
    width: 100%;
    height: 100%;
    color: #777;
    padding: 0 7px;
    position: relative;
    background-color: transparent;
    z-index: 2;
    border-radius: 2px;
    appearance: none;
    :focus {
      color: #000;
      outline: none;
    }
  }
`;
export const Icon = styled.div`
  display: none;
  position: relative;
  i {
    margin-top: 4px;
    font-size: 20px;
    cursor: pointer;
  }

  @media (max-width: 768px) {
    display: block;
  }
`;
export const DropMenu = styled.div`
  position: absolute;
  top: 30px;
  right: 0px;
  min-width: 300px;
  background-color: #0007;
  max-width: 400px;
  display: ${(props) => (props.show ? "block" : "none")};
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.4);
  text-align: center;
  padding: 10px;
  button {
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.4);
    width: 200px;
    height: 40px;
    font-size: 20px;
  }
  .games {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    font-size: 30px;
    font-weight: bold;
    margin-bottom: 20px;
    .game {
      background-color: rgb(250, 251, 251);
      color: #1890ff;
      cursor: pointer;
      width: 100px;
      height: 100px;
      margin: 5px;
      display: flex;
      justify-content: center;
      align-items: center;
      border-radius: 10px;
      :hover {
        background-color: #ddd;
      }
    }
  }
`;
export const Done = styled.div`
  position: fixed;
  min-width: 250px;
  max-width: 100%;
  display: ${(props) => (props.done ? "block" : "none")};
  color: #fff;
  background-color: #0007;
  bottom: 350px;
  left: 20px;
`;
