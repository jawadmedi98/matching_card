import styled, { keyframes } from "styled-components";

const speedOpen = "0.5s";
const ballspeed = "0.5s";
const afterFall = "1.7s";
const rotateScene = keyframes`
to{
    transform:rotateY(-360deg)
}
`;
const ballFalls = keyframes`
0%{
    top: -5em;
visibility: visible
}

100%{
    top: -0.5em;
    
}`;
const leftOpen = keyframes`
0%{
    transform: rotateY(90deg) translateZ(1em);
}

100%{
    transform:rotateY(90deg) translateZ(1em) rotateX(-89deg);
   
}
`;
const rightOpen = keyframes`
0%{
    transform: rotateY(270deg) translateZ(1em);
}

100%{
    transform:rotateY(270deg) translateZ(1em) rotateX(-89deg);
    
}
`;
const frontOpen = keyframes`
0%{
    transform: translateZ(1em);
}

100%{
    transform: translateZ(1em) rotateX(-89deg);
  

}
`;
const backOpen = keyframes`
0%{
    transform: rotateY(360deg) translateZ(-1em);
}

100%{
    transform: rotateY(360deg) translateZ(-1em) rotateX(89deg);
}
`;

const backColor = keyframes`
0%{
    background-color:rgb(24, 144, 255)
}
100%{
    background-color: rgb(24, 144, 255)
}
`;
export const Main = styled.div`
  width: 450px;
  height: 450px;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 45px;
  perspective: 10em;
  perspective-origin: 50% calc(50% - 3.5em);
  color: #fff;
  /* position: fixed;
  top: 0;
  left: 0;
  z-index: 3; */
`;
export const Scope = styled.div`
  position: relative;
  transform-style: preserve-3d;
  transform: rotateY(-30deg);
  animation: ${rotateScene} 50s linear infinite;
  .floor {
    width: 10em;
    height: 10em;
    background-image: radial-gradient(#5558, #0002),
      repeating-conic-gradient(
        #000 0deg 90deg,
        #f8f8f8 90deg 180deg,
        #000 180deg 270deg,
        #f8f8f8 270deg 360deg
      );

    background-size: 100% 100%, 2em 2em;
    transform: translate(-50%, -50%) rotateX(90deg);
    position: absolute;
    top: 1em;
    border-radius: 50%;
  }
  .box {
    top: -1em;
    left: -1em;
    width: 2em;
    height: 2em;
    position: absolute;
    transform-style: preserve-3d;
    .front,
    .left,
    .right,
    .back,
    .down {
      width: 100%;
      height: 100%;
      background: #0008;
      position: absolute;
      border: 6px solid #f8f8f8;
      display: flex;
      justify-content: center;
      align-items: center;
    }
    .front {
      transform: translateZ(1em);
      transform-origin: 100% 100%;
      animation: ${speedOpen} ${frontOpen} 1.5s linear forwards,
        0.2s ${backColor} ${afterFall} forwards;
    }
    .left {
      transform: rotateY(90deg) translateZ(1em);
      transform-origin: 50% 100%;
      animation: 0s ${backColor} ${afterFall} forwards,
        ${speedOpen} ${leftOpen} 2s linear forwards;
    }
    .right {
      transform: rotateY(270deg) translateZ(1em);
      transform-origin: 50% 100%;
      animation: ${speedOpen} ${rightOpen} 2.5s linear forwards,
        0.2s ${backColor} ${afterFall} forwards;
    }
    .back {
      transform: rotateY(360deg) translateZ(-1em);
      transform-origin: 50% 100%;
      animation: ${speedOpen} ${backOpen} 3s linear forwards,
        0.2s ${backColor} ${afterFall} forwards;
    }
    .down {
      transform: translateY(1em) rotateX(90deg);
      animation: ${speedOpen} ${backColor} 1.5s linear forwards,
        0.2s ${backColor} ${afterFall};
    }
    .up {
      transform: translateY(-1em) rotateX(90deg);
      /* background: #ffd9; */
    }
  }
  .ball {
    /* top: -2em; */
    left: -0.5em;
    width: 1em;
    height: 1em;
    background: lightblue;
    position: absolute;
    border-radius: 50%;
    background-image: radial-gradient(circle at top, lightblue, #000a);
    transform: rotateY(30deg);
    animation: ${ballspeed} ${ballFalls} 1s linear;
    visibility: hidden;
  }
`;
