import styled, { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
*{
    padding:0;
    margin:0;
    box-sizing: border-box;
    font-family:'Nunito', sans-serif;
    
}
`;

export const Container = styled.div`
  background: rgb(250, 251, 251);
  min-height: 100vh;
  text-align: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: 100%;
`;
export const H2 = styled.h2`
  margin-bottom: 40px;
  letter-spacing: 2px;
  @media (max-width: 1060px) {
    margin: 20px 10px 10px;
  }
`;

export const Field = styled.div`
  display: flex;
  justify-content: center;
  flex-wrap: wrap-reverse;
  align-items: flex-end;
`;
export default GlobalStyle;
