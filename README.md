# matching_card

### The design of this game was taken from this image

![image info](./pre.png)

## The game components

### Loading

During the game loading there is a 3d animation consists of three elements:

- The rotating table
- The box
- The ball
  ![image info](./gif.gif)

### Control Board
- Choose the number of cards from the select menu.
- Restart the game from the restart button.

![image info](./cbd.gif)

## Add later
- Get different images in every new game from an external database
- user authentication
- register user score 
- add closed games that unlock according to the user level

